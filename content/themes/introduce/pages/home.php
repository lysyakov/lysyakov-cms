<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lysyakov CMS</title>
    <link rel="stylesheet" href="<?php echo $this->theme->getUrl(); ?>/style.css">
</head>
<body>

    <article class="article">

        <h1>Lysyakov CMS Установлена</h1>

        <p>
            Вы только что успешно установили ядро системы управления
            сайтом, этого достаточно для создания сайта со статическим
            содержимым, описанным в отдельных файлах.
        </p>

        <h2>
            Установка тем оформления
        </h2>

        <p>
            Сейчас ядро загрузило этот шаблон, так как
            тема "introduce" установлена в качестве
            шаблона по умолчанию.
        </p>

    </article>

</body>
</html>
