<?php

/**
 * Database settings.
 */

return [
    'host'     => 'localhost', // Database host.
    'db_name'  => '',          // Database name.
    'username' => '',          // Database user name.
    'password' => '',          // Database user password.
    'charset'  => 'utf8'       // Database charset.
];
