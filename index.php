<?php

/**
 * Lysyakov CMS index file.
 * https://gitlab.com/lysyakov/lysyakov-cms
 */

/* Error reporting. */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/* Main defines. */
define("ROOT_DIR", __DIR__);
define('ENV', 'Frontend');

/* Require bootstrap file. */
require_once 'engine/bootstrap.php';

