<?php

/**
 * Frontend.
 */

namespace Engine;

use Engine\Helper\Common;
use Engine\Core\Router\DispatchedRoute;

class Frontend
{
    private $di;

    public $db;

    public $router;
    
    public $view;
    
    public $config;
    
    public $request;
    
    public $load;

    /**
     * Frontend constructor.
     */
    public function __construct($di)
    {
        /* Set dependency. */
        $this->di      = $di;
        $this->db      = $this->di->get('db');
        $this->router  = $this->di->get('router');
        $this->view    = $this->di->get('view');
        $this->config  = $this->di->get('config');
        $this->request = $this->di->get('request');
        $this->load    = $this->di->get('load');
    }

    /**
     * Run engine.
     */
    public function run()
    {
        try {

            /* Require routes file. */
            require_once __DIR__ . '/../' . mb_strtolower(ENV) . '/Route.php';
    
            /* Get router dispatch. */
            $routerDispatch = $this->router->dispatch(Common::getMethod(), Common::getPathUrl());
    
            /* If router dispatch was not found. */
            if ($routerDispatch == null) {

                /* Init error 404 router dispatch. */
                $routerDispatch = new DispatchedRoute('ErrorController:page404');

            }
    
            list($class, $action) = explode(':', $routerDispatch->getController(), 2);
    
            /* Controller namespace. */
            $controller = '\\' . ENV . '\\Controller\\' . $class;
            $parameters = $routerDispatch->getParameters();
    
            /* Call Controller->action. */
            call_user_func_array([new $controller($this->di), $action], $parameters);

            
    
        } catch (\ErrorException $e) {

            /* Echo error message and exit. */
            echo $e->getMessage();
            exit();

        }
    }
}
