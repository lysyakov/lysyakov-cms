<?php

/**
 * Radisher abstract Model.
 */

namespace Engine;

use Engine\Core\Database\QueryBuilder;
use Engine\DI\DI;

abstract class Model
{
    protected $di;

    protected $db;

    protected $view;

    protected $config;

    protected $request;

    protected $queryBuilder;
    
    /**
     * Abstract Model constructor.
     */
    public function __construct(DI $di)
    {
        /* Set dependency. */
        $this->di      = $di;
        $this->db      = $this->di->get('db');
        $this->view    = $this->di->get('view');
        $this->config  = $this->di->get('config');
        $this->request = $this->di->get('request');

        /* New QueryBuilder. */
        $this->queryBuilder = new QueryBuilder();
    }
}