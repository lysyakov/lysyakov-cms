<?php

/**
 * Load Service Provider.
 */

namespace Engine\Service\Load;

use Engine\Service\AbstractProvider;
use Engine\Load;

class Provider extends AbstractProvider
{
    /* Service name. */
    public $serviceName = 'load';

    /**
     * Init provider.
     */
    public function init(){

        /* New Load. */
        $load = new Load($this->di);

        /* Set Load in DI. */
        $this->di->set($this->serviceName, $load);
    }
}
