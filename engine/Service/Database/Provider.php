<?php

/**
 * Database Service Provider.
 */

namespace Engine\Service\Database;

use Engine\Service\AbstractProvider;
use Engine\Core\Database\Connection;

class Provider extends AbstractProvider
{
    /* Service name. */
    public $serviceName = 'db';

    /**
     * Init provider.
     */
    public function init(){

        /* New Connection. */
        $db = new Connection();

        /* Set Connection in DI. */
        $this->di->set($this->serviceName, $db);
    }
}
