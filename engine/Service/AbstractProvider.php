<?php

/**
 * Service abstract Provider.
 */

namespace Engine\Service;

abstract class AbstractProvider
{
    protected $di;

    /**
     * AbstractProvider constructor.
     */
    public function __construct($di)
    {
        $this->di = $di;
    }

    /**
     * Abstract init service function.
     */
    abstract function init();
}
