<?php

/**
 * Config Service Provider.
 */

namespace Engine\Service\Config;

use Engine\Service\AbstractProvider;
use Engine\Core\Config\Config;

class Provider extends AbstractProvider
{
    /* Service name. */
    public $serviceName = 'config';

    /**
     * Init provider.
     */
    public function init(){

        /* Get configs. */
        $config['main']     = Config::file('main');
        $config['database'] = Config::file('database');

        /* Set configs in DI. */
        $this->di->set($this->serviceName, $config);

    }
}
