<?php

/**
 * Router Service Provider.
 */

namespace Engine\Service\Router;

use Engine\Service\AbstractProvider;
use Engine\Core\Router\Router;

class Provider extends AbstractProvider
{
    /* Service name. */
    public $serviceName = 'router';

    /**
     * Init provider.
     */
    public function init(){

        /* New Router. */
        $router = new Router('http://rediska.io/');

        /* Set Router in DI. */
        $this->di->set($this->serviceName, $router);
    }
}
