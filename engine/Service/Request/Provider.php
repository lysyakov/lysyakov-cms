<?php

/**
 * Request Service Provider.
 */

namespace Engine\Service\Request;

use Engine\Service\AbstractProvider;
use Engine\Core\Request\Request;

class Provider extends AbstractProvider
{
    /* Service name. */
    public $serviceName = 'request';

    /**
     * Init provider.
     */
    public function init(){

        /* New Request. */
        $request = new Request();

        /* Set Request in DI. */
        $this->di->set($this->serviceName, $request);
    }
}
