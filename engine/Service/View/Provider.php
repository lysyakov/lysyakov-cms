<?php

/**
 * View Service Provider.
 */

namespace Engine\Service\View;

use Engine\Service\AbstractProvider;
use Engine\Core\Template\View;

class Provider extends AbstractProvider
{
    /* Service name. */
    public $serviceName = 'view';

    /**
     * Init provider.
     */
    public function init(){

        /* New View. */
        $view = new View($this->di);

        /* Set View in DI. */
        $this->di->set($this->serviceName, $view);
    }
}
