<?php

/**
 * Radisher abstract Controller.
 */

namespace Engine;

use Engine\DI\DI;
use Engine\Core\Auth\Auth;

abstract class Controller
{
    protected $di;

    protected $db;

    protected $view;

    protected $config;

    protected $request;

    protected $load;

    protected $auth;
    
    public function __construct(DI $di)
    {
        /* Get dependency. */
        $this->di      = $di;
        $this->db      = $this->di->get('db');
        $this->view    = $this->di->get('view');
        $this->config  = $this->di->get('config');
        $this->request = $this->di->get('request');
        $this->load    = $this->di->get('load');

        $this->initVars();

        $this->auth = new Auth();
    }


    public function __get($key)
    {
        return $this->di->get($key);
    }

    public function initVars()
    {
        $vars = array_keys(get_object_vars($this));

        foreach ($vars as $var) {
            if ($this->di->has($var)) {
                $this->{$var} = $this->di->get($var);
            }
        }

        return $this;
    }

    /**
     * Check authorization.
     */
    public function checkAuthorization()
    {
        if($this->auth->hashUser() !== null) {

            /* User authorize. */
            $this->auth->authorize($this->auth->hashUser());
            
        }
        
        if(!$this->auth->authorized()) {

            /* Redirect to login. */
            header('Location: /signin');
            exit();

        }
    }
}
