<?php

/**
 * DI Container.
 */

namespace Engine\DI;

class DI
{
    /* DI Container. */
    private $container = [];

    /**
     * Set dependency in DI.
     */
    public function set($key, $value) 
    {
        $this->container[$key] = $value;

        return $this;
    }

    /**
     * Get dependency from DI.
     */
    public function get($key)
    {
        return $this->has($key);
    }

    /**
     * Push dependency in DI.
     */
    public function push($key, $element = [])
    {
        if ($this->has($key) !== null) {
            $this->set($key, []);
        }

        if (!empty($element)) {
            $this->container[$key][$element['key']] = $element['value'];
        }
    }

    /**
     * Has dependency in DI.
     */
    public function has($key)
    {
        return isset($this->container[$key]) ? $this->container[$key] : [];
    }
    
}
