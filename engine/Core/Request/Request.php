<?php

/**
 * Request.
 */

namespace Engine\Core\Request;

class Request
{
    public $get = [];

    public $post = [];

    public $request = [];

    public $cookie = [];

    public $files = [];

    public $server = [];

    /**
     * Request constructor.
     */
    public function __construct()
    {
        $this->get     = _htmlspecialchars($_GET);
        $this->post    = _htmlspecialchars($_POST);
        $this->request = $_REQUEST;
        $this->cookie  = $_COOKIE;
        $this->files   = $_FILES;
        $this->server  = $_SERVER;
    }
}
