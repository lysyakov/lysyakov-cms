<?php

/**
 * Auth.
 */

namespace Engine\Core\Auth;

use Engine\Helper\Cookie;

class Auth implements AuthInterface
{
    protected $authorized = false;

    protected $hashUser;

    /**
     * Is authorized.
     */
    public function authorized()
    {
        return $this->authorized;
    }

    /**
     * Get hash user from Cookie.
     */
    public function hashUser()
    {
        return Cookie::get('auth_hashUser');
    }

    /**
     * Authorize user.
     */
    public function authorize($hashUser)
    {
        Cookie::set('auth_authorized', true);
        Cookie::set('auth_hashUser', $hashUser);

        $this->authorized = true;
        $this->hashUser   = $hashUser;
    }

    /**
     * Unauthorize user.
     */
    public function unAuthorize()
    {
        Cookie::delete('auth_authorized');
        Cookie::delete('auth_hashUser');

        $this->authorized = false;
        $this->hashUser   = null;
    }
    
    /**
     * Salt.
     */
    public function salt()
    {
        return (string) rand (10000000, 99999999);
    }

    /**
     * Encrypt password.
     */
    public static function encryptPassword($password, $salt = '')
    {
        return hash('sha256', $password . $salt);
    }
}
