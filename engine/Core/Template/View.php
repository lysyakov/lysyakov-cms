<?php

/**
 * View Template.
 */

namespace Engine\Core\Template;

use Engine\Core\Template\Theme;
use Engine\Core\Config\Config;

class View
{
    /* DI Container. */
    protected $di;

    /* Theme. */
    protected $theme;

    /**
     * View constructor.
     */
    public function __construct($di)
    {
        /* Get di. */
        $this->di = $di;

        /* New Theme. */
        $this->theme = new Theme();
    }

    /**
     * Render page.
     */
    public function render($template, $vars = [])
    {
        include_once $this->getThemePath() . '/functions.php';

        $templatePath = $this->getTemplatePath($template, ENV);

        if (!is_file($templatePath)) {
            throw new \InvalidArgumentException(sprintf('Template %s not found in %s', $template, $templatePath));
        }

        /* Get languages. */
        $vars['lang'] = $this->di->get('language');

        /* Send global data in Theme. */
        $this->theme->setData($vars);

        /* Extract vars global data. */
        extract($vars);

        ob_start();
        ob_implicit_flush(0);

        try {

            /* Require template. */
            require $templatePath;

        } catch (\Exception $e) {

            /* Echo error message. */
            ob_end_clean();
            echo $e->getMessage();

        }

        /* Print template. */
        echo ob_get_clean();
    }

    /**
     * Get template path.
     */
    private function getTemplatePath($template, $env = false)
    {
        if($env == 'Frontend') {
            return ROOT_DIR . '/content/themes/' . Config::item('defaultTheme', 'main') . '/pages/' . $template . '.php';
        }

        /* Get path to View folder. */
        return path('view') . '/' . $template . '.php';
    }

    /**
     * Get theme path.
     */
    private function getThemePath()
    {
        return ROOT_DIR . '/content/themes/' . Config::item('defaultTheme', 'main');
    }
}
