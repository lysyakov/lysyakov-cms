<?php

/**
 * Theme Template.
 */

namespace Engine\Core\Template;

use Engine\Core\Config\Config;

class Theme
{
    /* Name file rules. */
    const RULES_NAME_FILE = [
        'header'  => 'header-%s',
        'footer'  => 'footer-%s',
        'sidebar' => 'sidebar-%s'
    ];

    /* Url theme mask. */
    const URL_THEME_MASK = '%s/content/themes/%s';

    protected static $url = '';

    /* Global data. */
    protected static $data = [];

    /* Asset. */
    public $asset;

    /* Theme. */
    public $theme;

    /* Component. */
    public $component;

    /**
     * Theme constructor.
     */
    public function __construct()
    {
        /* New Asset. */
        $this->asset = new Asset();

        /* This. */
        $this->theme = $this;

        /* New Component. */
        $this->component = new Component();
    }

    /**
     * Get url.
     */
    public static function getUrl()
    {
        $currentTheme = Config::item('defaultTheme', 'main');
        $baseUrl      = Config::item('baseUrl', 'main');

        return sprintf(self::URL_THEME_MASK, $baseUrl, $currentTheme);
    }

    /**
     * Header.
     */
    public function header($name = null)
    {
        $name = (string) $name;
        $file = self::detectNameFile($name, __FUNCTION__);

        $this->component->load($file);
    }

    /**
     * Footer.
     */
    public function footer($name = null)
    {
        $name = (string) $name;
        $file = self::detectNameFile($name, __FUNCTION__);

        $this->component->load($file);
    }

    /**
     * Sidebar.
     */
    public function sidebar($name = nul)
    {
        $name = (string) $name;
        $file = self::detectNameFile($name, __FUNCTION__);

        $this->component->load($file);
    }

    /**
     * Block.
     */
    public function block($name = '', $data = [])
    {
        $name = (string) $name;

        if ($name !== '') {
            $this->component->load($name, $data);
        }
    }

    /**
     * Get global data.
     */
    public static function getData()
    {
        return static::$data;
    }

    /**
     * Set global data.
     */
    public static function setData($data)
    {
        static::$data = $data;
    }

    /**
     * Detect filename by function name.
     */
    private static function detectNameFile($name, $function)
    {
        return empty(trim($name)) ? $function : sprintf(self::RULES_NAME_FILE[$function], $name);
    }

    /**
     * Get theme path.
     */
    public static function getThemePath()
    {
        return ROOT_DIR . '/content/themes/default';
    }
}
