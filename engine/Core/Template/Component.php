<?php

/**
 * Component Template.
 */

namespace Engine\Core\Template;

class Component
{
    /**
     * Load component.
     */
    public function load($name, $data = [])
    {
        /* Path to theme folder. */
        $templateFile = ROOT_DIR . '/content/themes/default/blocks/' . $name . '.php';

        if (ENV == 'Admin') {

            /* Get path to View folder. */
            $templateFile = path('view') . '/' . $name . '.php';

        }

        if (is_file($templateFile)) {

            /* Extract global data. */
            extract(array_merge($data, Theme::getData()));

            /* Require template. */
            require_once $templateFile;

        } else {

            /* Echo error message. */
            throw new \Exception(
                sprintf('View file does not exists %s', $templateFile)
            );

        }
    }
}
