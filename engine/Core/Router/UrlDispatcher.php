<?php

/**
 * Router UrlDispatcher.
 */

namespace Engine\Core\Router;

/**
 * @category Engine.
 * @package Core.
 * @subpackage Router.
 */
class UrlDispatcher
{
    /**
     * Список методов запроса.
     *
     * @var array
     */
    private array $methods = [
        'GET',
        'POST'
    ];

    /**
     * Список роутов, принадлежащих какому-либо методу запроса.
     *
     * @var array
     */
    private array $routes = [
        'GET'  => [],
        'POST' => []
    ];

    /**
     * Undocumented variable
     *
     * @var array
     */
    private array $patterns = [
        'int' => '[0-9]+',
        'str' => '[a-zA-Z\.\-_%]+',
        'any' => '[a-zA-Z0-9\.\-_%]+'
    ];

    /**
     * Добавление паттерна.
     *
     * @param string $key
     * @param string $pattern
     * @return void
     */
    public function addPattern(string $key, string $pattern)
    {
        $this->patterns[$key] = $pattern;
    }

    public function dispatch(string $method, string $uri)
    {
        $routes = $this->routes(strtoupper($method));

        if (array_key_exists($uri, $routes)) {
            return new DispatchedRoute($routes[$uri]);
        }

        return $this->doDispatch($method, $uri);
    }

    public function register(string $method, string $pattern, string $controller)
    {
        $convert = $this->convertPattern($pattern);

        $this->routes[strtoupper($method)][$convert] = $controller;
    }

    /**
     * Конвертирование параметров.
     *
     * @param [type] $pattern
     * @return void
     */
    private function convertPattern($pattern)
    {
        if (strpos($pattern, '(') === false) {
            return $pattern;
        }

        return preg_replace_callback('#\((\w+):(\w+)\)#', [$this, 'replacePattern'], $pattern);
    }

    /**
     * Замена параметров по правилу.
     *
     * @param [type] $matches
     * @return void
     */
    private function replacePattern($matches)
    {
        return '(?<' .$matches[1]. '>'. strtr($matches[2], $this->patterns) .')';
    }

    /**
     * Получение списка роутов, принадлежащих какому-либо методу запроса.
     *
     * @param string $method
     * @return void
     */
    private function routes(string $method)
    {
        return isset($this->routes[$method]) ? $this->routes[$method] : [];
    }

    private function doDispatch(string $method, string $uri)
    {
        foreach ($this->routes($method) as $route => $controller) {
            $pattern = '#^' . $route . '$#s';

            if (preg_match($pattern, $uri, $parameters)) {
                return new DispatchedRoute($controller, $this->processParam($parameters));
            }
        }
    }

    /**
     * Удаление лишних ключей в параметрах.
     *
     * @param [type] $parameters
     * @return void
     */
    private function processParam($parameters)
    {
        foreach ($parameters as $key => $value) {
            if (is_int($key)) {
                unset($parameters[$key]);
            }
        }

        return $parameters;
    }
}