<?php

/**
 * Router DispatchedRoute.
 */

namespace Engine\Core\Router;

class DispatchedRoute
{
    /* Controller. */
    private $controller;

    /* Parameters. */
    private $parameters;

    /**
     * DispatchedRoute constructor.
     */
    public function __construct($controller, $parameters = [])
    {
        /* Set controller. */
        $this->controller = $controller;

        /* Set parameters. */
        $this->parameters = $parameters;
    }

    /**
     * Get controller.
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * Get parameters.
     */
    public function getParameters()
    {
        return $this->parameters;
    }
}
