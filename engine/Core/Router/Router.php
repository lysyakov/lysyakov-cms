<?php

/**
 * Router.
 */

namespace Engine\Core\Router;

/**
 * @category Engine.
 * @package Core.
 * @subpackage Router.
 */
class Router
{
    /**
     * Список маршрутов.
     *
     * @var array
     */
    private $routes = [];

    /**
     * Хост.
     *
     * @var [type]
     */
    private $host;

    /**
     * Диспетчер.
     *
     * @var [type]
     */
    private $dispatcher;

    /**
     * Router конструктор.
     *
     * @param string $host
     */
    public function __construct(string $host)
    {
        $this->host = $host;
    }

    /**
     * Добавление маршрута.
     *
     * @param string $key
     * @param string $pattern
     * @param string $controller
     * @param string $method
     * @return void
     */
    public function add(string $key, string $pattern, string $controller, string $method = 'GET')
    {
        $this->routes[$key] = [
            'pattern'    => $pattern,
            'controller' => $controller,
            'method'     => $method
        ];
    }

    /**
     * @param string $method
     * @param string $uri
     * @return void
     */
    public function dispatch(string $method, string $uri)
    {
        return $this->getDispatcher()->dispatch($method, $uri);
    }

    /**
     * Получение диспетчера.
     *
     * @return void
     */
    public function getDispatcher()
    {
        if ($this->dispatcher == null) {
            $this->dispatcher = new UrlDispatcher();

            foreach ($this->routes as $route) {
                $this->dispatcher->register($route['method'], $route['pattern'], $route['controller']);
            }
        }

        return $this->dispatcher;
    }
}