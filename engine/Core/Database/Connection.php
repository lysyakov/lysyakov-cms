<?php

/**
 * Database Connection.
 */

namespace Engine\Core\Database;

use \PDO;
use Engine\Core\Config\Config;

class Connection
{
    private $link;

    /**
     * Connection constructor.
     */
    public function __construct()
    {
        $this->connect();
    }

    /**
     * Connect to database.
     */
    private function connect()
    {
        /* Get database config. */
        $config = Config::group('database');

        $dsn = 'mysql:host='.$config['host'].';dbname='.$config['db_name'].';charset='.$config['charset'];

        $this->link = new PDO($dsn, $config['username'], $config['password']);

        return $this;
    }

    /**
     * Execute request.
     */
    public function execute($sql, $values = [])
    {
        $sth = $this->link->prepare($sql);

        return $sth->execute($values);
    }

    /**
     * Query request.
     */
    public function query($sql, $values = [], $statement = PDO::FETCH_OBJ)
    {
        $sth = $this->link->prepare($sql);

        $sth->execute($values);

        $result = $sth->fetchAll($statement);

        if($result === false) {
            return [];
        }

        return $result;
    }

    /**
     * Return last insert id.
     */
    public function lastInsertId()
    {
        return $this->link->lastInsertId();
    }
}
