<?php

/**
 * Radisher functions library.
 */

/**
 * Get path for folder.
 */
function path($section)
{
    $pathMask = ROOT_DIR . '/' . '%s';

    if (ENV == 'Frontend') {
        $pathMask = ROOT_DIR . '/' . strtolower(ENV) . '/' . '%s';
    }

    switch (strtolower($section)) {
        case 'controller':
            return sprintf($pathMask, 'Controller');
        case 'config':
            return ROOT_DIR . '/config/';
        case 'model':
            return sprintf($pathMask, 'Model');
        case 'view':
            return sprintf($pathMask, 'View');
        case 'language':
            return sprintf($pathMask, 'Language');
        default:
            return ROOT_DIR;
    }
}

/**
 * Get languages list.
 */
function languages()
{
    $directory = path('language');
    $list      = scandir($directory);
    $languages = [];

    if (!empty($list)) {
        unset($list[0]);
        unset($list[1]);

        foreach ($list as $dir) {
            $pathLangDir = $directory . '/' . $dir;
            $pathConfig  = $pathLangDir . '/config.json';
            if (is_dir($pathLangDir) and is_file($pathConfig)) {
                $config = file_get_contents($pathConfig);
                $info   = json_decode($config);

                $languages[] = $info;
            }
        }
    }

    return $languages;
}

/**
 * Time converter.
 */
function timeStampConverter($primaryDate)
{    
    $date1 = new DateTime($primaryDate);
    $date2 = new DateTime();

    /* Set current date and time. */
    $date2->setTimestamp(time() + (60 * 60));

    /* Calculate dates difference. */
    $interval = $date1->diff($date2);

    if ($interval->days == 0) {

        if ($interval->h == 0) {

            if ($interval->i < 5) return 'Сейчас';

            if ($interval->i < 10) return 'Недавно';

            return $interval->i . ' мин назад';
        }

        if ($interval->h > 0) return $interval->h . ' ч назад';
    }

    return $interval->days . ' дней ' . $interval->h . ' ч назад';
}

/**
 * Get path to users avatar.
 */
function getUserAvatar($avatar)
{
    if (isset($avatar) and (!empty($avatar))) {
        return '/content/uploads/' . $avatar;
    }

    return '/content/themes/default/assets/image/avatar_default_1.png';
}

/**
 * Modern htmlspecialchars.
 */
function _htmlspecialchars($data = [])
{
    $charsedData = [];

    foreach($data as $newData => $value) {
        $charsedData[$newData] = htmlspecialchars($value);
    }

    return $data;
}
