<?php

/**
 * Radisher Common helper.
 */

namespace Engine\Helper;

class Common
{
    /**
     * Is request method post.
     */
    public static function isPost()
    {
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            return true;
        }
        
        return false;
    }

    /**
     * Get request method.
     */
    public static function getMethod()
    {
        return $_SERVER['REQUEST_METHOD'];
    }

    /**
     * Get path url.
     * Will modified.
     */
    public static function getPathUrl()
    {
        $pathUrl = $_SERVER['REQUEST_URI'];

        /* Cut url string before get parameters. */
        if ($position = strpos($pathUrl, '?')) {
            $pathUrl = substr($pathUrl, 0, $position);
        }

        return $pathUrl;
    }
}
