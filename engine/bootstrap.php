<?php

/**
 * Radisher bootstrap.
 */

/* Require PSR-4 autoload. */
require_once __DIR__ . '/../vendor/autoload.php';

/* Require functions library. */
require_once __DIR__ . '/Function.php';

class_alias('Engine\\Core\\Template\Asset', 'Asset');
class_alias('Engine\\Core\\Template\Theme', 'Theme');

use Engine\Frontend;
use Engine\DI\DI;

try{

    /* New DI Container. */
    $di = new DI();

    /* Get services list. */
    $services = require __DIR__ . '/Config/Service.php';

    /* Init services. */
    foreach ($services as $service) {

        $provider = new $service($di);
        $provider->init();

    }

    /* Run Frontend. */
    $frontend = new Frontend($di);
    $frontend->run();

} catch (\ErrorException $e) {

    /* Echo error message. */
    echo $e->getMessage();

}
