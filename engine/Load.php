<?php

/**
 * Load.
 */

namespace Engine;

class Load
{
    /* Model entity mask.
       Will deleted. */
    const MASK_MODEL_ENTITY     = '\%s\Model\%s\%s';

    /* Model repository mask. */
    const MASK_MODEL_REPOSITORY = '\%s\Model\%s\%sRepository';

    /* Languages file mask. */
    const FILE_MASK_LANGUAGE    = 'Language/%s/%s.ini';

    public $di;

    /**
     * Load constructor.
     */
    public function __construct($di)
    {
        $this->di = $di;

        return $this;
    }

    /**
     * Load model by name.
     */
    public function model($modelName, $modelDir = false)
    {
        $modelName = ucfirst($modelName);
        $modelDir = $modelDir ? $modelDir : $modelName;

        $namespaceModel = sprintf(
            self::MASK_MODEL_REPOSITORY, ENV, $modelDir, $modelName
        );

        $isClassModel = class_exists($namespaceModel);

        if ($isClassModel) {

            /* Model as new stdClass. */
            $modelRegistry = $this->di->get('model') ?: new \stdClass();
            $modelRegistry->{lcfirst($modelName)} = new $namespaceModel($this->di);

            /* Set model in DI. */
            $this->di->set('model', $modelRegistry);

        }

        return $isClassModel;
    }

    /**
     * Load languages.
     */
    public function language($path)
    {
        $file = sprintf(
            self::FILE_MASK_LANGUAGE,
            'english', $path
        );

        /* Get content language file. */
        $content = parse_ini_file($file, true);

        /* Formating language name. */
        $languageName = $this->toCamelCase($path);

        /* Language as new stdClass. */
        $language = $this->di->get('language') ?: new \stdClass();
        $language->{$languageName} = $content;

        /* Set languages in DI. */
        $this->di->set('language', $language);

        /* Return content language file. */
        return $content;
    }

    /**
     * Formating as CamelCase.
     */
    private function toCamelCase($str)
    {
        $replace = preg_replace('/[^a-zA-Z0-9]/', ' ', $str);
        $convert = mb_convert_case($replace, MB_CASE_TITLE);
        $result  = lcfirst(str_replace(' ', '', $convert));

        return $result;
    }
}
