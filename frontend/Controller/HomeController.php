<?php

/**
 * HomeController.
 */

namespace Frontend\Controller;

class HomeController extends FrontendController
{
    /**
     * Home page.
     */
    public function index()
    {
        $this->view->render('home');
    }
}
