<?php

/**
 * FrontendController.
 */

namespace Frontend\Controller;

use Engine\Controller;

class FrontendController extends Controller
{

    /* Global data. */
    public $data = [];

    /**
     * FrontendController constructor.
     */
    public function __construct($di)
    {
        /* User parent (Controller) constructor. */
        parent::__construct($di);

    }
}
